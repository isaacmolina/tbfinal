class Competidor
	attr_accessor :categoria, :dni,:nombre, :peso, :tiempo, :faltas
	def initialize(categoria, dni, nombre, peso, tiempo, faltas)
		@categoria=categoria # esta en numeros  (1:amateur, 2:profesional, 3:master)
		@dni=dni
		@nombre=nombre
		@peso=peso
		@tiempo=tiempo
		@faltas=faltas
	end 
	def calculaTiempo
		tiempo + faltas * 3
	end
	def presentarse		
		"   \t\tdni        : #{dni}
		nombre     : #{nombre}
		peso       : #{peso}
		tiempo     : #{tiempo}
		faltas     : #{faltas} "
	end
end

class Amateur < Competidor 
	def initialize(categoria, dni, nombre, peso, tiempo, faltas)
		super(categoria, dni, nombre, peso, tiempo, faltas)
	end
	def presentarse
		"   Categoria  : Amateur\n" + super
	end 
end

class Profesional < Competidor
	def initialize(categoria, dni, nombre, peso, tiempo, faltas)
		super(categoria, dni, nombre, peso, tiempo, faltas)
	end
	def presentarse
		"   Categoria  : Profesional\n" + super
	end 
end
class Master < Competidor
	attr_accessor :edad
	def initialize(categoria, dni, nombre, peso, tiempo, faltas, edad)
		super(categoria, dni, nombre, peso, tiempo, faltas)
		@edad = edad
	end
	def calculaTiempo
		tiempoFinal = super
		if edad >= 55
			tiempoFinal = super - 2
		end
		return tiempoFinal
	end
	def presentarse
		"   Categoria  : Master\n" + super + "\n\t\tedad       : #{edad} "
	end 
end

class Competencia
	attr_accessor :competidores, :auxiliarMaster 
	def initialize
		@competidores=[]
	end
	def agregar( categoria, dni, nombre, peso, tiempo, faltas, edad)
		if categoria == 1 # Amateur
			competidor = Amateur.new(categoria, dni, nombre, peso, tiempo, faltas)
		elsif categoria == 2 # Profesional
			competidor = Profesional.new(categoria, dni, nombre, peso, tiempo, faltas)
		elsif categoria == 3 # Master
			competidor = Master.new(categoria, dni, nombre, peso, tiempo, faltas, edad)
			@auxiliarMaster = competidor
		else 
			puts "categoria no existe"
			return false
		end
		competidores.push(competidor)
	end
	def menorTiempo
		menor =  competidores[0]
		for i in 0..competidores.size-1
			if competidores[i].calculaTiempo < competidores[0].calculaTiempo
				menor = competidores[i]
			end
		end
		puts "El MENOR TIEMPO FINAL OBTENIDO ES PARA:"
		puts menor.presentarse
	end
	def menorTiempoMasters
		menor = auxiliarMaster
		for i in 0..competidores.size-1
			if competidores[i].categoria == 3
				if competidores[i].calculaTiempo < competidores[0].calculaTiempo
					menor = competidores[i]
				end
			end
		end
		puts "El MENOR TIEMPO FINAL OBTENIDO EN LA CATEGORIA MASTER ES:"
		puts menor.presentarse
	end
	def listar
		puts "# -----  POMPETIDORES  -----#"
		for i in 0..competidores.size-1
			puts competidores[i].presentarse.to_s
			puts "    Tiempo Final: #{competidores[i].calculaTiempo}"
			puts "-------------------------------"
		end
	end 
end 

competencia=Competencia.new 
competencia.agregar( 3, '45678925', 'victor Molina', 70, 500, 0, 28)
competencia.agregar( 3, '45678925', 'Juan', 70, 500, 0, 58)
competencia.agregar( 1, '45678925', 'Jose', 70, 1000, 0, 20)
competencia.agregar( 1, '45678925', 'Jose', 70, 1000, 5, 20)
competencia.agregar( 2, '45678925', 'Natalia', 70, 1010, 5, 20)
competencia.agregar( 2, '45678925', 'Cesar', 70, 20, 5, 20)
competencia.agregar( 3, '45678925', 'Alicia', 70, 10, 5, 20)
#competidores precargados
respuesta='s'
while respuesta=='s'
	puts "Elija una opción"
	puts "(1) Registrar Competidores"
	puts "(2) Listar Competidores"

	puts "(3) Mejor Tiempo"
	puts "(4) Menor tiempo Master"
	opcion=gets.to_i
	if opcion == 1

		puts "Ingrese categoria de acuerdo a la tabla" 
		puts "(1) Amateur"
		puts "(2) Profesiona"
		puts "(3) Master"
		categoria = gets.to_i
		puts "Ingrese dni" 
		dni = gets.chomp
		puts "Ingrese nombre" 
		nombre = gets.chomp
		puts "Ingrese peso" 
		peso = gets.to_f
		puts "Ingrese tiempo de competencia (en segundos)" 
		tiempo = gets.to_i
		puts "Ingrese número de faltas" 
		faltas = gets.to_i
		edad = nil
		if categoria == 3 
			puts "Ingrese Edad" 
			faltas = gets.to_i
		end

		competencia.agregar( categoria, dni, nombre, peso, tiempo, faltas, edad)
	elsif opcion == 2
		competencia.listar
	elsif opcion == 3
		competencia.menorTiempo
	elsif opcion == 4
		competencia.menorTiempoMasters

	else
		puts "Opción no valida"
	end
	puts "Desea realizar otra operación? s/n"
	respuesta=gets.chomp
end
figura.listar