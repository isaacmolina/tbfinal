#arr = [110, 55, 103, 86, 32, 12, 82, 43, 1]
#
#		num = arr.size - 1
#		for i in 0..num
#			j = i
#			for i in 0..num
#				if arr[i] < arr[j]
#					aux = arr[i]
#					arr[i] = arr[j]
#					arr[j] = aux
#				end
#			end
#		end
#puts  arr
#HELPERS
def getEstado(estado)
	estados = ['PENDIENTE', 'INGRESO', 'NO INGRESO']
	return estados[estado]
end
#VISTAS
class HelperView
	def mensaje(msg)
		puts msg
	end
end
class PersonaView
	def mostrar(persona)		
		puts "\ndni         : #{persona.dni}"
		puts "nombres     : #{persona.nombres}"
		puts "apellidos   : #{persona.apellidos}"
		
	end
end
class PostulanteView < PersonaView
	def mostrar(postulante)
		super
		colegio = postulante.procedencia == 1 ? 'Nacional' : 'Privado'
		puts "edad        : #{postulante.edad}"
		puts "genero      : #{postulante.genero}"
		puts "procedencia : Colegio #{colegio}"
	end
end
class PostulanteNacionalView < PostulanteView
	def mostrar(postulante)
		super
		zona = postulante.zona == 1 ? 'Rural' : 'Urbana'
		puts "zona        : #{zona}"
		puts "ponderado   : #{postulante.ponderado}"
	end
end
class PostulantePrivadoView < PostulanteView
	def mostrar(postulante)
		super
		puts "puesto      : #{postulante.puesto}"
		puts "pension     : #{postulante.pension}"
	end
end
class TutorView < PersonaView
	def mostrar(tutor)
		super
		puts "parentesco : #{tutor.parentesco}"
	end
end
class ProcesoAdmisionView
	def listarPostulantes(fichas)
		if fichas.size == 0
			puts "No hay postulantes registrados\n"
			return false
		end
		postulanteNacionalView = PostulanteNacionalView.new
		postulantePrivadoView = PostulantePrivadoView.new
		for i in 0..fichas.size-1
			if fichas[i].postulante.procedencia == 1
				postulanteNacionalView.mostrar(fichas[i].postulante)
			else
				postulantePrivadoView.mostrar(fichas[i].postulante)
			end
			puts "estado     : #{getEstado(fichas[i].estado)}"
		end	
	end
	def mostrarTutores(ficha)
		if !ficha || !ficha.tutores
			puts "No hay Tutores registrados\n"
			return false
		end
		tutorView = TutorView.new
		for i in 0..ficha.tutores.size-1
			tutorView.mostrar(ficha.tutores[i])
		end
		#falta mostrar el examen con sus preguntas y el puntaje final
	end	
	def mostrarPostulante(ficha)
		if !ficha
			puts "No hay postulantes registrados\n"
			return false
		end
		if ficha.postulante.procedencia == 1
			postulanteNacionalView = PostulanteNacionalView.new
			postulanteNacionalView.mostrar(ficha.postulante)
		else
			postulantePrivadoView = PostulantePrivadoView.new
			postulantePrivadoView.mostrar(ficha.postulante)
		end
		if ficha.puntaje
			puts "\nPuntaje     : #{ficha.puntaje}"
		end
		puts "estado      : #{getEstado(ficha.estado)}\n"
		puts "evalucaiones      : ec: #{ficha.ec} re: #{ficha.re}cs: #{ficha.cs}\n"
		#falta mostrar el examen con sus preguntas y el puntaje final
	end
end
#MODELOS
class Pregunta
	attr_accessor :enunciado, :alternativas, :respuestaCorrecta
	def initialize(enunciado, alternativas, respuestaCorrecta)
		@enunciado = enunciado 
		@alternativas = alternativas 
		@respuestaCorrecta = respuestaCorrecta
	end
end
class Examen
	attr_accessor :codigoExamen, :tipo, :numPreguntas, :preguntas
	def initialize(codigoExamen, tipo, numPreguntas)
		@codigoExamen = codigoExamen
		@tipo = tipo
		@numPreguntas = numPreguntas
		@preguntas = []
	end
	def agregarPregunta(enunciado, alternativas, respuestaCorrecta)
		pregunta = Pregunta.new(enunciado, alternativas, respuestaCorrecta)
		@preguntas.push(pregunta)
	end
end
class EvaluacionConocimiento
	attr_accessor :codigoEC, :examen, :respuestas, :correctas, :incorrectas, :sinresponder
	def initialize(codigoEC, examen, respuestas)
		@codigoEC = codigoEC
		@examen = examen
		@respuestas = respuestas
		@correctas = correctas
		@incorrectas = incorrectas
		@sinresponder = sinresponder
	end
	def calcularPuntaje
	end
end
class Persona
	attr_accessor  :dni,:nombres, :apellidos
	def initialize(dni, nombres, apellidos)
		@dni=dni
		@nombres=nombres
		@apellidos=apellidos
	end
end
class Postulante < Persona
	attr_accessor :procedencia, :edad, :genero
	def initialize(dni, nombres, apellidos, edad, genero, procedencia)
		super(dni, nombres, apellidos)
		@procedencia = procedencia
		@edad=edad
		@genero=genero
	end
end
class PostulanteNacional < Postulante
	attr_accessor :ponderado, :zona 
	def initialize(dni, nombres, apellidos, edad, genero, procedencia, ponderado, zona)
		super(dni, nombres, apellidos, edad, genero, procedencia)
		@zona = zona # 1: Rural , 2: Urbana
		@ponderado = ponderado
	end
	def calificacionSocioEconomica
		zona == 1 ? 100 : 80
	end
	def calificacionPorRendimiento
			puntaje =100
		if ponderado >= 18 and ponderado < 19
			puntaje = 80
		elsif ponderado >= 16 and ponderado < 18
			puntaje = 60
		elsif ponderado >= 14 and ponderado < 16
			puntaje = 40
		elsif ponderado >= 11 and ponderado <14
			puntaje = 20
		else 
			puntaje = 0
		end
		return puntaje
	end

end
class PostulantePrivado < Postulante
	attr_accessor :puesto, :pension
	def initialize(dni, nombres, apellidos, edad, genero, procedencia, puesto, pension)
		super(dni, nombres, apellidos, edad, genero, procedencia)
		@puesto = puesto
		@pension = pension
	end
	def calificacionSocioEconomica
		puntos = 0
		if 200 >= pension 
			puntos = 90
		elsif (200 < pension and pension <= 400)
			puntos = 70
		elsif 400 < pension and pension <= 600
			puntos = 50
		else
			puntos = 40				
		end
		return puntos
	end
	def calificacionPorRendimiento
		puntaje = 0
		
		if puesto <= 3
			puntaje = 100
		elsif puesto >= 4 and puesto < 6
			puntaje = 80
		elsif puesto >=6 and puesto < 11
			puntaje = 60
		elsif puesto >= 11 and puesto < 21
			puntaje = 40
		else
			puntaje = 0
		end	
		return puntaje
	end
end
class Tutor < Persona
	attr_accessor :parentesco
	def initialize(dni, nombres, apellidos, parentesco)
		super(dni, nombres, apellidos)
		@parentesco = parentesco
	end
end

#CONTROLADORES
class PersonaController
	def initialize(dni, nombres, apellidos)
		@personaModel = Persona.new(dni, nombres, apellidos)
		@personaView = PersonaView.new
	end
	def mostrar
		@personaView.mostrar(@personaModel)
	end
end

class PostulanteController < PersonaController
	def initialize(dni, nombres, apellidos, edad, genero, procedencia)
		super(dni, nombres, apellidos)
		@postulanteModel = Postulante.new(dni, nombres, apellidos, edad, genero, procedencia)
		@postulanteView = PostulanteView.new
	end
	def mostrar
		@postulanteView.mostrar(@postulanteModel)
	end
end

class PostulanteNacionalController < PostulanteController
	def initialize(dni, nombres, apellidos, edad, genero, procedencia, ponderado, zona)
		super(dni, nombres, apellidos, edad, genero, procedencia)
		@postulanteNacionalModel = PostulanteNacional.new(dni, nombres, apellidos, edad, genero, procedencia, ponderado, zona )
		@postulanteNacionalView = PostulanteNacionalView.new
	end
	def calificacionSocioEconomica
		@postulanteNacionalModel.calificacionSocioEconomica
	end
	def calificacionPorRendimiento
		@postulanteNacionalModel.calificacionPorRendimiento
	end
	def mostrar
		@postulanteNacionalView.mostrar(@postulanteNacionalModel)
	end
end
class PostulantePrivadoController < PostulanteController
	def initialize(dni, nombres, apellidos, edad, genero, procedencia, puesto, pension)
		super(dni, nombres, apellidos, edad, genero, procedencia)
		@postulantePrivadoModel = PostulantePrivado.new(dni, nombres, apellidos, edad, genero, procedencia, puesto, pension )
		@postulantePrivadoView = PostulantePrivadoView.new
	end
	def calificacionSocioEconomica
		@postulantePrivadoModel.calificacionSocioEconomica
	end
	def calificacionPorRendimiento
		@postulantePrivadoModel.calificacionPorRendimiento
	end
	def mostrar
		@postulantePrivadoView.mostrar(@postulantePrivadoModel)
	end
end

class TutorController < PersonaController
	def initialize(dni, nombres, apellidos, parentesco)
		super(dni, nombres, apellidos)
		@tutorModel = Tutor.new(dni, nombres, apellidos, parentesco)
		@tutorView = TutorView.new
	end
	def mostrar
		@tutorView.mostrar(@tutorModel)
	end
end

#test MVC
#postulante = PostulanteController.new('dni', 'nombres', 'apellidos', 'edad', 'genero', 'procedencia')
#postulante.mostrar()


class FichaAdmision
	attr_accessor :postulante, :tutores, :ec, :re, :cs, :puntaje, :estado, :evaluacionConocimiento
	def initialize(aspirante, tutores = [], cs, re)
		@postulante = aspirante
		@tutores = tutores
		@re = re #rendimiento 2do Grado
		@cs = cs #ficha socioeconomica
		@estado = 0 # 0:PENDIENTE, 1:INGRESA, 2:NO INGRESA
		@ec = 0  #ec: evaluacion academica
	end
	def agregarEvalucionAcademica(ev)
		@evaluacionConocimiento = ev
	end
	def rendirEvaluacion
		@ec = 80 
		return 100
	end
	def agregarTutor()
	end
	def calcularPuntaje()
		@puntaje = cs * 0.2 + re * 0.3 + ec * 0.5
		return @puntaje
	end
	def setEstado(estado)
		@estado = estado
	end
end
class ProcesoAdmision
	attr_accessor :numVacantes, :fichasAdmision, :evaluados
	def initialize(numVacantes)
		@numVacantes = numVacantes
		@fichasAdmision = []
		@evaluados = []
	end
	def agregarPostulante(dni, nombres, apellidos, edad, genero, procedencia, tutores, zona, pension, ponderado, puesto)
		# seleciono postulante por procedencia 1:nacional, 2:PRIVADO
		if procedencia == 1
			postulante = PostulanteNacional.new(dni, nombres, apellidos, edad, genero, procedencia, ponderado, zona)
		else
			postulante = PostulantePrivado.new(dni, nombres, apellidos, edad, genero, procedencia, puesto, pension)
		end
			
		cs = postulante.calificacionSocioEconomica()
		re = postulante.calificacionPorRendimiento
		ficha = FichaAdmision.new(postulante, tutores, cs, re)
		@fichasAdmision.push(ficha)
	end
	def agregarEvalucionAcademica(dni, ev)
		ficha = buscarPostulanteXDNI(dni)
		#ficha.agregarEvalucionAcademica(ev)
	end
	def rendirEvaluacion(dni)
		ficha = buscarPostulanteXDNI(dni)
		if ficha
			ficha.rendirEvaluacion()
			ficha.calcularPuntaje()
		end
	end
	def buscarPostulanteXDNIApoderado(dni)
		tempfichas = []
		for i in 0..fichasAdmision.size-1
			for f in 0..fichasAdmision[i].tutores.size-1
				if fichasAdmision[i].tutores[f].dni == dni
					#if fichasAdmision[i].tutores.dni == dni
					tempfichas.push(fichasAdmision[i])
				end
			end
		end
		return tempfichas
	end
	def buscarPostulanteXDNI(dni)
		ficha = nil
		for i in 0..fichasAdmision.size-1
			if fichasAdmision[i].postulante.dni == dni
				ficha = fichasAdmision[i]
			end
		end
		return ficha
	end
	def listarEvaluados
	end
	def ordenarEvaluados(arrayEvaluados, key)
		# Algoritmo Ordenamiento de Burbuja
		#a = [110, 55, 103, 86, 32, 12, 82, 43, 1]

		num = arrayEvaluados.size - 1
		for i in 0..num
			j = i
			for i in 0..num
				if arrayEvaluados[i].puntaje < arrayEvaluados[j].puntaje
					aux = arrayEvaluados[i]
					arrayEvaluados[i] = arrayEvaluados[j]
					arrayEvaluados[j] = aux
				end
			end
		end
		return arrayEvaluados
	end
	def procesarEvaluaciones
		#numVacantes,
		#Seleccion de evaluedos
		arrayEvaluados = []
		for i in 0..fichasAdmision.size-1
			if fichasAdmision[i].ec > 0
				fichasAdmision[i].calcularPuntaje
				arrayEvaluados.push(fichasAdmision[i])
			end
		end
		#ordenamieto
		arrayEvaluados = self.ordenarEvaluados(arrayEvaluados, 'puntaje')
		#calificacion segun vacantes
		for i in 0..arrayEvaluados.size-1
			estado = i < numVacantes ? 1 : 2
			arrayEvaluados[i].setEstado(estado)
		end
		@evaluados = arrayEvaluados
		return arrayEvaluados
	end
	def cantidadPostulantesPorSexo
		masculino = 0
		femeninio = 0
		for i in 0..fichasAdmision.size-1
			if fichasAdmision[i].postulante.genero == 'masculino'
				masculino += 1
			else
				femeninio += 1
			end
		end
		return [masculino, femeninio]
	end	
	def cantidadIngresantesPorSexo
		masculino = 0
		femeninio = 0
		for i in 0..evaluados.size-1
			if evaluados[i].estado == 1
				if evaluados[i].postulante.genero == 'masculino'
					masculino += 1
				else
					femeninio += 1
				end
			end
		end
		return [masculino, femeninio]
	end	
	def cantidadNoIngresantesPorSexo
		masculino = 0
		femeninio = 0
		for i in 0..evaluados.size-1
			if evaluados[i].estado == 2
				if evaluados[i].postulante.genero == 'masculino'
					masculino += 1
				else
					femeninio += 1
				end
			end
		end
		return [masculino, femeninio]
	end
end

class ProcesoAdmisionController
	def initialize(vacantes)
		@ProcesoAdmisionModel = ProcesoAdmision.new(vacantes)
		@ProcesoAdmisionView = ProcesoAdmisionView.new
		@helperView = HelperView.new
	end
	def agregarPostulante(dni, nombres, apellidos, edad, genero, procedencia, tutores, zona, pension, ponderado, puesto)
		@ProcesoAdmisionModel.agregarPostulante(dni, nombres, apellidos, edad, genero, procedencia, tutores, zona, pension, ponderado, puesto)
	end
	def listarPostulantes
		@ProcesoAdmisionView.listarPostulantes(@ProcesoAdmisionModel.fichasAdmision)
	end
	def consultarPostulante(dni)
		ficha = @ProcesoAdmisionModel.buscarPostulanteXDNI(dni)
		@ProcesoAdmisionView.mostrarPostulante(ficha)
	end	
	def agregarEvalucionAcademica(dni, ev)
		@ProcesoAdmisionModel.agregarEvalucionAcademica(dni, ev)
	end	
	def consultarPostulantePorApoderado(dni)
		fichas = @ProcesoAdmisionModel.buscarPostulanteXDNIApoderado(dni)
		if fichas.size == 0
			@helperView.mensaje("No Hay Registros encontrados")
			return false
		end

		for i in 0..fichas.size - 1
			@ProcesoAdmisionView.mostrarPostulante(fichas[i])
		end
	end
	def consultarTutores(dni)
		ficha = @ProcesoAdmisionModel.buscarPostulanteXDNI(dni)
		@ProcesoAdmisionView.mostrarTutores(ficha)
	end
	def rendirEvaluacion(dni)
		@ProcesoAdmisionModel.rendirEvaluacion(dni)
	end
	def listarEvaluados
		evaluados = @ProcesoAdmisionModel.procesarEvaluaciones
		if evaluados.size == 0
			@helperView.mensaje('No se a Tomado la Evaluacion Academica, para ningun postulante')
			return false
		end
		for i in 0..evaluados.size - 1
			@ProcesoAdmisionView.mostrarPostulante(evaluados[i])
		end
	end
	def listarIngresantes
		fichas = @ProcesoAdmisionModel.evaluados
		mostrados = 0
		for i in 0..fichas.size - 1
			if fichas[i].estado == 1
				@ProcesoAdmisionView.mostrarPostulante(fichas[i])
				mostrados += 1
			end
		end
		if mostrados == 0
			@helperView.mensaje("\nNo hay  Ingresantes")
		end
	end
	def listarNoIngresantes
		fichas = @ProcesoAdmisionModel.evaluados
		mostrados = 0
		for i in 0..fichas.size - 1
			if fichas[i].estado == 2
				@ProcesoAdmisionView.mostrarPostulante(fichas[i])
				mostrados += 1
			end
		end
		if mostrados == 0
			@helperView.mensaje("\nNo hay Postulantes Rechazados")
		end
	end
	def procesarEvaluaciones
		@ProcesoAdmisionModel.procesarEvaluaciones
	end
	def cantidadPostulantesSexo
		cantidad = @ProcesoAdmisionModel.cantidadPostulantesPorSexo
		@helperView.mensaje("\nCantidad de Postulantes:\nHombres #{cantidad[0]}\n Cantidad de Mujeres: #{cantidad[1]}")
	end
	def cantidadIngresantesSexo
		cantidad = @ProcesoAdmisionModel.cantidadIngresantesPorSexo
		@helperView.mensaje("\nCantidad de Ingresantes:\nHombres #{cantidad[0]}\n Cantidad de Mujeres: #{cantidad[1]}")
	end
	def cantidadNoIngresantesSexo
		cantidad = @ProcesoAdmisionModel.cantidadNoIngresantesPorSexo
		@helperView.mensaje("\nCantidad de No Ingresantes (rechazados):\nHombres #{cantidad[0]}\n Cantidad de Mujeres: #{cantidad[1]}")
	end
end


#Data Precargada
examen = nil
#Precarga de Examen
examen=Examen.new('0001', 1, 10)
#pregunta 1
enunciado = "1. En las moléculas de agua, los enlaces  puentes de hidrógeno son:"
alternativas = [
"A.	Se forman entre hidrógenos de distintas moléculas.",
"B. Se forman entre los hidrógenos y los oxígenos de la misma molécula de agua.",
"C. Se forman entre los hidrógenos y los oxígenos de distintas moléculas de agua.",
"D. Son enlaces iónicos"]
respuestaCorrecta = 'B'
examen.agregarPregunta(enunciado, alternativas, respuestaCorrecta)
#pregunta 2
enunciado = "2.	¿Qué parte de los ácidos grasos es hidrófoba?"
alternativas = [
"A.	El grupo carboxilo",
"B. La cadena hidrocarbonada",
"C. El grupo amino",
"D. Todo el conjunto"]
respuestaCorrecta = 'B'
examen.agregarPregunta(enunciado, alternativas, respuestaCorrecta)
#pregunta 3
enunciado = "3. Los ácidos grasos son anfipáticos..."
alternativas = [
"A.	Porque caen mal en las comidas copiosas",
"B. Por tener una parte polar y otra apolar",
"C. Por tener dobles enlaces conjugados",
"D. Por tener una parte con carga negativa y otra con carga positiva"]
respuestaCorrecta = 'C'
examen.agregarPregunta(enunciado, alternativas, respuestaCorrecta)
examen.agregarPregunta(enunciado, alternativas, respuestaCorrecta)
#pregunta 4
examen.agregarPregunta(" 4 Enunciado...", alternativas, respuestaCorrecta)
examen.agregarPregunta(" 5 Enunciado...", alternativas, respuestaCorrecta)
examen.agregarPregunta(" 6 Enunciado...", alternativas, respuestaCorrecta)
examen.agregarPregunta(" 7 Enunciado...", alternativas, respuestaCorrecta)
examen.agregarPregunta(" 8 Enunciado...", alternativas, respuestaCorrecta)
examen.agregarPregunta(" 9 Enunciado...", alternativas, respuestaCorrecta)
examen.agregarPregunta(" 10 Enunciado...", alternativas, respuestaCorrecta)

#Admisión
respuesta='s'
puts "######################################################"
puts "#         Bienveniod al Sistema de Admisión          #"
puts "######################################################"
puts " Por Favor Ingrese el número de vacantes disponibles"
vacantes = gets.to_i


procesoAdmision = ProcesoAdmisionController.new(vacantes)
#data precargada
#postulantes pre cargados
tutor = Tutor.new('11111111', 'tutornombres', 'tutorapellidos', 'papá')
procesoAdmision.agregarPostulante('123456789', 'Victor', 'apellidos', 25, 'masculino', 1 , [tutor], 1, 1500, 15, 1)
tutor = Tutor.new('22222222', 'tutornombres', 'tutorapellidos', 'papá')
procesoAdmision.agregarPostulante('789456123', 'Samuel', 'apellidos', 22, 'masculino', 2 , [tutor], 2, 2000, 16, 3)
tutor = Tutor.new('33333333', 'tutornombres', 'tutorapellidos', 'papá')
procesoAdmision.agregarPostulante('456789123', 'Juan', 'apellidos', 22, 'masculino', 2 , [tutor], 2, 2000, 16, 3)
tutor = Tutor.new('44444444', 'tutornombres', 'tutorapellidos', 'papá')
procesoAdmision.agregarPostulante('123789456', 'Ana', 'apellidos', 22, 'femenino', 2 , [tutor], 2, 2000, 16, 3)
tutor = Tutor.new('44444444', 'tutornombres', 'tutorapellidos', 'papá')
procesoAdmision.agregarPostulante('456123789', 'Victor', 'apellidos', 22, 'masculino', 2 , [tutor], 2, 2000, 16, 3)
tutor = Tutor.new('55555555', 'tutornombres', 'tutorapellidos', 'papá')
mama =  Tutor.new('66666666', 'tutornombres', 'tutorapellidos', 'madre')
procesoAdmision.agregarPostulante('741852963', 'Miguel', 'apellidos', 22, 'masculino', 1 , [tutor], 1, 2000, 20, 3)
procesoAdmision.agregarPostulante('852963741', 'Jazmín', 'apellidos', 22, 'femenino', 2 , [tutor], 2, 180, nil, 1)
#rendir evaluación
procesoAdmision.rendirEvaluacion('123456789')
procesoAdmision.rendirEvaluacion('789456123')
procesoAdmision.rendirEvaluacion('456789123')
procesoAdmision.rendirEvaluacion('123789456')
procesoAdmision.rendirEvaluacion('456123789')
procesoAdmision.rendirEvaluacion('741852963')
procesoAdmision.rendirEvaluacion('852963741') 
while respuesta=='s' || respuesta=='S'
	puts "Elija una operación"
	puts "(1) Registrar Postulante"
	puts "(2) Registrar Exámen"

	puts "(3) Consultar Postulante" #rúbrica
	puts "(4) Rendir Evaluación de Conocimiento"
	puts "(5) Listar Resultados de Evaluaciones" #rubrica
	puts "(6) Listar Postulantes"		
	puts "(7) Reportes"		
	puts "(8) Consultar Tutores" #rúbrica
	puts "(9) Procesar Evaluaciones" #rúbrica
		
	opcion=gets.to_i
	if opcion == 1
		#pide datos del postulante
		ponderado = 0
		zona = 0
		puesto = 0
		pension = 0
		puts "Ingrese dni"
		dni=gets.chomp
		puts "Ingrese nombres"
		nombres=gets.chomp
		puts "Ingrese apellidos"
		apellidos=gets.chomp
		puts "Ingrese edad"
		edad=gets.chomp
		puts "Ingrese genero (masculino, femenino)"
		genero=gets.chomp
		puts "Ingrese tipo de Procedencia del colegio"
		puts "(1) Nacional"
		puts "(2) Privado"
		tipoColegio=gets.to_i

		if tipoColegio == 1
			puts "Ingrese el promedio ponderado del 2do Año"
			ponderado = gets.to_i
			puts "Ingrese Zona en la que se encuentra el colegio"
			puts "(1) Rural"
			puts "(2) Urbano"
			zona = gets.to_i
		else 
			puts "Ingrese el puesto que ocupó durante el 2do Año"
			puesto = gets.to_i
			puts "Ingrese El monto del pago mensual por pensión al colegio"
			pension = gets.to_f
		end
		
		#pide datos del tutor
		agregarTutor = 's'
		tutores = []

		while agregarTutor == 's'
			puts "\nAgregar Tutor"
			#pide datos de los tutores
			puts "Ingrese DNI del Tutor"
			dni=gets.to_i
			puts "Ingrese Nombre del Tutor"
			nombre=gets.chomp
			puts "Ingrese Apellidos del Tutor"
			apellidos=gets.chomp
			puts "Ingrese parentesco"
			parentesco=gets.chomp
			tutor = Tutor.new(dni, nombre, apellidos, parentesco)
			tutores.push(tutor)
			puts "Desea agregar otro tutor? s/n"
			agregarTutor=gets.chomp
		end
		procesoAdmision.agregarPostulante(dni, nombres,  apellidos,  edad,  genero,  tipoColegio , tutores, zona, pension, ponderado, puesto)
	elsif opcion == 2
		puts "Elija un tipo de Examen"
		puts "(1) de 10 preguntas "
		puts "(2) de 20 preguntas "
		tipo = gets.to_i
		puts "Código del Éxamen"
		codigoExamen = gets.chomp
		if tipo == 1
			numPreguntas  = 10
		else 
			numPreguntas  = 20
		end
		examen=Examen.new(codigoExamen, tipo, numPreguntas)
		# registro de preguntas y alternativas
		for i in 1..numPreguntas #numpreguntas 10
			#numPreguntas[i].examen.agregarPregunta
			#registro de la pregunta 4
			puts "Ingrese la pregunta #{i}"
			enunciado = gets.chomp
			#registro de alternativas pregunta 4
			puts "Ingrese Alternativa A"
			a = gets.chomp
			puts "Ingrese Alternativa B"
			b = gets.chomp
			puts "Ingrese Alternativa C"
			c = gets.chomp
			puts "Ingrese Alternativa D"
			d = gets.chomp

			alternativas = [a,b,c,d]
			#respuesta correcta
			control1 = true
			while control1
				puts "Ingrese la letra de la respuesta correcta: (A, B, C, D)"
				rpc = gets.chomp
				if rpc == 'A' ||  rpc == 'B'||  rpc == 'C'||  rpc == 'D' 
					examen.agregarPregunta(enunciado, alternativas, rpc)
					control1 = false
				else
					puts "La letra ingresada es invalida"
				end
			end
		end

	elsif opcion == 3
		puts "Por DNI del Alumno"
		dni = gets.chomp
		procesoAdmision.consultarPostulante(dni)
		puts "Por DNI del Apoderado"
		dni = gets.chomp
		procesoAdmision.consultarPostulantePorApoderado(dni)
	elsif opcion == 4
		if examen == nil
			puts "No hay exámenes ingresados, registra un examen"
		else 
			puts "Ingrese Su DNI"
			codigoEC = gets.chomp
			respuestas = []
			for i in 0..examen.numPreguntas - 1
				puts examen.preguntas[i].enunciado
				for f  in 0..examen.preguntas[i].alternativas.size - 1
					puts examen.preguntas[i].alternativas[f]
				end
				#respuesta correcta
				control2 = true
				while control2
					puts "Ingrese la letra de la respuesta correcta: (A, B, C, D)"
					rpc = gets.chomp
					if rpc == 'A' ||  rpc == 'B'||  rpc == 'C'||  rpc == 'D' 
						respuestas.push(rpc)
						control2 = false
					else
						puts "La letra ingresada es invalida"
					end
				end
			end
			#instancia e valuación
			evaluacion = EvaluacionConocimiento.new(codigoEC, examen, respuestas)
			procesoAdmision.agregarEvalucionAcademica(dni, evaluacion)
		end
		
	elsif opcion == 5
		puts "\nSeleccione un tipo de evaluación:"
		puts "(1) Todos los alumnos"
		puts "(2) Alumnos Ingresantes"	
		puts "(3) Alumnos No Ingresantes"	
		tipo=gets.to_i
		if tipo  == 1
			procesoAdmision.listarEvaluados()
		elsif tipo  == 2
			procesoAdmision.listarIngresantes()
		elsif tipo  == 3
			procesoAdmision.listarNoIngresantes()
		else
			puts "Opción no valida"
		end
			
	elsif opcion == 6
		procesoAdmision.listarPostulantes()
	elsif opcion == 7
		puts "\nSeleccione un tipo de reporte:"
		puts "(1) Cantidad de alumnos postulantes masculinos y femeninos."
		puts "(2) Ingresantes masculinos y femeninos"
		puts "(3) No ingresantes masculinos y femeninos"
		puts "(4) Ingresantes porcentaje de colegios nacionales y particulares."
		puts "(5) No ingresantes porcentaje de colegios nacionales y particulares."
		puts "(6) La edad de los estudiantes se encuentra entre 11 – 15 años, mostrar"
		puts "(7) reporte cantidad de ingresantes y no ingresante por edades."
		tipo=gets.to_i
		procesoAdmision.procesarEvaluaciones
		if tipo == 1
			procesoAdmision.cantidadPostulantesSexo()
		elsif tipo == 2
			procesoAdmision.cantidadIngresantesSexo()
		elsif tipo == 3
			procesoAdmision.cantidadNoIngresantesSexo()
		elsif tipo == 4
			puts "Opción en desarrollo"
		elsif tipo == 5
			puts "Opción en desarrollo"
		elsif tipo == 6
			puts "Opción en desarrollo"
		elsif tipo == 7
			puts "Opción en desarrollo"
		else
			puts "Opción no valida"
		end
	elsif opcion == 8
		procesoAdmision.consultarTutores('789123456')		
	elsif opcion == 9
		procesoAdmision.procesarEvaluaciones
	else
		puts "Opción no valida"
	end
	puts "Desea realizar otra operación? s/n"
	respuesta=gets.chomp
end


