class Postulante
	attr_accessor :colegio, :dni, :apellidos, :nombres, :edad, :genero
	def initialize(colegio,dni,apellidos,nombres,edad,genero)
	@colegio=colegio # 1.Nacional 2.Particular
	@dni=dni
	@apellidos=apellidos
	@nombres=nombres
	@edad=edad
	@genero=genero
	end
end

class Nacional < Postulante
	attr_accessor :tipo, :promedio
	def initialize(colegio,dni,apellidos,nombres,edad,genero,tipo,promedio)
	@tipo=tipo # A.Rural B.Urbano
	@promedio=promedio
	end
	def calificacion_socieconomica
		if tipo == "A"
			puntaje=100
		elsif tipo == "B"
			puntaje=80
		else
			puts "No encontrado"
			return false
		end
	end
	def rendimiento
		if promedio >= 19
			puntaje_rendimiento=100
		elsif promedio >= 18 and promedio < 19
			puntaje_rendimiento=80
		elsif promedio >= 16 and promedio < 18
			puntaje_rendimiento=60
		elsif promedio >= 14 and promedio < 16
			puntaje_rendimiento=40
		elsif promedio >= 11 and promedio <14
			puntaje_rendimiento=20
		elsif puntaje_rendimiento < 11
			puntaje_rendimiento=0
		end
	end
	
	def evaluacion_conocimiento
	end
end

class Particular < Postulante
	attr_accessor :pension, :puesto
	def initialize(colegio,dni,apellidos,nombres,edad,genero,promedio,pension,puesto)
	@pension=pension
	@puesto=puesto #1.primer puesto 2.segundo puesto 3.tercer puesto....20.veinte puesto
	end
	def calificacion_socieconomica
		if pension <= 200
			puntaje=90
		elsif pension > 200 and pension <= 400
			puntaje=70
		elsif pension > 400 and pension <= 600
			puntaje=50
		elsif pension > 600
			puntaje=40
		end
	end
	def rendimiento
		if puesto > 0 and puesto <= 3
			puntaje_rendimiento=100
		elsif puesto >= 4 and puesto < 6
			puntaje_rendimiento=80
		elsif puesto >=6 and puesto < 11
			puntaje_rendimiento=60
		elsif puesto >= 11 and puesto < 21
			puntaje_rendimiento=40
		elsif puesto > 20
			puntaje_rendimiento=0
		end		
	end
	def evaluacion_conocimiento
	end
end

class Tutor
	attr_accessor :dni_tutor, :apellidos_tutor, :nombres_tutor, :parentesco
	def initialize(dni_tutor,apellidos_tutor,nombres_tutor,parentesco)
	@dni_tutor=dni_tutor
	@apellidos_tutor=apellidos_tutor
	@nombres_tutor=nombres_tutor
	@parentesco=parentesco
	end
end

class Evaluacion
	attr_accessor :codigo, :numero_preguntas
	def initialize(codigo,pregunta)
	@codigo=codigo #1.a (contiene 20 preguntas) 2.b (contiene 10 preguntas)
	@pregunta=pregunta
	end
end

puts "Ingrese el codigo de la evaluacion"
	codigo=gets.chomp
puts "Ingrese la 1era pregunta"
	pregunta=gets.chomp