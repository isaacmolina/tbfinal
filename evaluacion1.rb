class Evaluacion
	attr_accessor :preguntas, :marcas , :respuestas
	def initialize(preguntas,marcas,respuestas)
		@preguntas=preguntas
		@marcas=marcas
		@respuestas=respuestas
	end
	
	def puntaje
	end
end

class Examen
	attr_accessor :agregar_preguntas, :agregar_marcas, :agregar_respuestas
	def initialize
		@agregar_preguntas=[]
		@agregar_marcas=[]
		@agregar_respuestas=[]
	end
	
	def agregar_pregunta(preguntas)
		agregar_preguntas.push(preguntas)
	end
	
	def agregar_marcas(marcas)
		agregar_marcas.push(marcas)
	end
	
	def agregar_respuesta(respuestas)
		agregar_respuestas.push(respuestas)
	end
end

evaluacion=Examen.new

pregunta_1 = evaluacion.agregar_pregunta("¿Quién escribió La Odisea?")
pregunta_2 = evaluacion.agregar_pregunta("¿Cuál es el río más largo del mundo?")
pregunta_3 = evaluacion.agregar_pregunta("¿Quién es el autor de el Quijote?")
pregunta_4 = evaluacion.agregar_pregunta("¿Qué año llegó Cristóbal Colón a América?")
pregunta_5 = evaluacion.agregar_pregunta(" ¿Cuál es el nombre de la lengua oficial en china?")
pregunta_6 = evaluacion.agregar_pregunta("¿Quién escribió La Odisea?")
pregunta_7 = evaluacion.agregar_pregunta("¿Quién escribió La Odisea?")
pregunta_8 = evaluacion.agregar_pregunta("¿Cuál es el gentilicio de la ciudad de Valladolid?")
pregunta_9 = evaluacion.agregar_pregunta("¿Quién fue el presidente estadounidense al inicio de la Segunda Guerra Mundial?")
pregunta_10 = evaluacion.agregar_pregunta("¿Cuál es el “País del Sol Naciente?")